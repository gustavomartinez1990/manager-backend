<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('priority')->default(2);
            $table->integer('type')->default(0);
            $table->integer('status')->default(2);
            $table->integer('duration')->default(0);
            $table->boolean('is_milestone')->default(false);
            $table->boolean('is_sub_task')->default(false);
            $table->integer('order')->default(0);
            $table->string('slug');
            $table->dateTime('due_date');
            $table->dateTime('started_at')->nullable();
            $table->dateTime('finished_at')->nullable();
            
            // If there is a task that needs to be finished before this task.
            $table->unsignedBigInteger('require_task_id')->nullable();
            $table->foreign('require_task_id')
                ->references('id')
                ->on('tasks')
                ->onDelete('set null');

            // If this is a subtask of another parent task.
            $table->unsignedBigInteger('parent_task_id')->nullable();
            $table->foreign('parent_task_id')
                ->references('id')
                ->on('tasks')
                ->onDelete('set null');

            // A task belongs to a sub_project.
            $table->unsignedBigInteger('sub_project_id');
            $table->foreign('sub_project_id')
                ->references('id')
                ->on('sub_projects');

            // A task can be assigned to a user.
            $table->unsignedBigInteger('assigned_to')->nullable();
            $table->foreign('assigned_to')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
