<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_projects', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');
            $table->integer('type')->default(0);
            $table->integer('status')->default(1);
            $table->integer('priority')->default(2);
            $table->integer('duration')->default(0);
            $table->integer('remaining_duration')->default(0);
            $table->integer('total_tasks')->default(0);
            $table->integer('remaining_tasks')->default(0);
            $table->double('cost')->default(0);
            $table->dateTime('due_date');
            $table->dateTime('start_date');
            $table->dateTime('started_at')->nullable();
            $table->dateTime('finished_at')->nullable();

            // A sub project belongs to a client.
            $table->unsignedBigInteger('project_id');
            $table->foreign('project_id')
                ->references('id')
                ->on('projects');

            // A sub project belongs to a client.
            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id')
                ->references('id')
                ->on('clients');

            // A sub project can be assigned to a user.
            $table->unsignedBigInteger('assigned_to')->nullable();
            $table->foreign('assigned_to')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_projects');
    }
}
