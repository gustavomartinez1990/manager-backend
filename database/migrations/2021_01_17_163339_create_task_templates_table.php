<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_templates', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('type');
            $table->integer('status')->default(2);
            $table->integer('duration')->default(0);
            $table->boolean('is_milestone')->default(false);
            $table->boolean('is_sub_task')->default(false);
            $table->integer('order')->default(0);
            $table->string('slug');

            // If there is a task that needs to be finished before this task.
            $table->unsignedBigInteger('require_task_id')->nullable();
            $table->foreign('require_task_id')
                ->references('id')
                ->on('task_templates')
                ->onDelete('set null');

            // If this is a subtask of another parent task.
            $table->unsignedBigInteger('parent_task_id')->nullable();
            $table->foreign('parent_task_id')
                ->references('id')
                ->on('task_templates')
                ->onDelete('set null');

            // A task template belongs to a sub_project.
            $table->unsignedBigInteger('sub_project_template_id')->nullable();
            $table->foreign('sub_project_template_id')
                ->references('id')
                ->on('sub_project_templates')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_templates');
    }
}
