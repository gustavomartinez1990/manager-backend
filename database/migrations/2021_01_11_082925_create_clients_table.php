<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('fiscal_name');
            $table->string('commercial_name')->nullable();
            $table->string('nif',100)->unique();
            $table->string('phone_number',30);
            $table->string('email');
            $table->string('logo_url')->nullable();
            
            // Address fields.
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('street_address')->nullable();
            $table->string('postal_code')->nullable();

            $table->integer('type');
            $table->integer('status');
            
            // For referral.
            $table->unsignedBigInteger('referral_id')->nullable();
            $table->foreign('referral_id')
                ->references('id')
                ->on('clients')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
