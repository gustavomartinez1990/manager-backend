<?php

namespace Database\Factories;

use App\Models\Project;
use App\Models\SubProject;
use App\Models\SubProjectPriority;
use App\Models\SubProjectStatus;
use App\Models\SubProjectType;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SubProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubProject::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $startDate = CarbonImmutable::now()->add(1, 'day');
        $dueDate = CarbonImmutable::now()->add(15, 'day');
        $name = $this->faker->sentence(3);
        $project = Project::first();

        return [
            'name' => $name,
            'description' => $this->faker->paragraph(2, false),
            'slug' => Str::slug($name, '-'),
            'type' => $this->faker->numberBetween(SubProjectType::PRODUCT, SubProjectType::SERVICE),
            'status' => $this->faker->numberBetween(SubProjectStatus::DISABLED, SubProjectStatus::ACTIVE),
            'priority' => $this->faker->numberBetween(SubProjectPriority::LOWEST, SubProjectPriority::HIGHEST),
            'duration' => 0,
            'remaining_duration' => 0,
            'total_tasks' => 0,
            'remaining_tasks' => 0,
            'cost' => $this->faker->randomFloat(2,50,500),
            'client_id' => $project->id,
            'project_id' => $project->client_id,
            'assigned_to' => 3,
            'due_date' => $dueDate,
            'start_date' => $startDate, 
        ];
    }
}
