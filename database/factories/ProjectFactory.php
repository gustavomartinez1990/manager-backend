<?php

namespace Database\Factories;

use App\Models\Project;
use App\Models\ProjectPriority;
use App\Models\ProjectStatus;
use App\Models\ProjectType;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(3),
            'description' => $this->faker->paragraph(2, false),
            'documentation_url' => $this->faker->url(),
            'type' => $this->faker->numberBetween(ProjectType::CONSULTANT, ProjectType::CONSULTANT),
            'status' => $this->faker->numberBetween(ProjectStatus::DISABLED, ProjectStatus::TODO),
            'priority' => $this->faker->numberBetween(ProjectPriority::LOWEST, ProjectPriority::HIGHEST),
            'total_sub_projects' => $this->faker->numberBetween(1, 3),
            'cost' => $this->faker->randomFloat(2,50,500),
            'client_id' => $this->faker->numberBetween(1, 3),
            'assigned_to' => $this->faker->numberBetween(1, 3)
        ];
    }
}
