<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\ClientStatus;
use App\Models\ClientType;

use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fiscal_name' => $this->faker->company(),
            'commercial_name' => $this->faker->catchPhrase(),
            'nif' => $this->faker->regexify('[A-Z]{5}[0-4]{3}'),
            'phone_number' => $this->faker->phoneNumber(),
            'email' => $this->faker->companyEmail(),
            'logo_url' => $this->faker->url(),
            'country' => $this->faker->countryCode(),
            'city' => $this->faker->city(),
            'street_address' => $this->faker->streetAddress(),
            'postal_code' => $this->faker->postcode(),
            'type' => $this->faker->numberBetween(ClientType::CLIENT, ClientType::RESELLER),
            'status' => $this->faker->numberBetween(ClientStatus::DISABLED, ClientStatus::DELETED)
        ];
    }
}
