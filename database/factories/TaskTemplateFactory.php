<?php

namespace Database\Factories;

use App\Models\TaskTemplate;

use App\Models\TaskStatus;
use App\Models\TaskType;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class TaskTemplateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TaskTemplate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence(3);
        return [
            'name' => $name,
            'description' => $this->faker->paragraph(),
            'type' => $this->faker->numberBetween(TaskType::NEW_TASK, TaskType::MAINTENANCE),
            'status' => $this->faker->numberBetween(TaskStatus::DISABLED, TaskStatus::DONE),
            'duration' => $this->faker->randomNumber(3, false),
            'is_milestone' => $this->faker->boolean(),
            'is_sub_task' => $this->faker->boolean(),
            'order' => $this->faker->randomNumber(1, false),
            'slug' => Str::slug($name, '-'),
            'require_task_id' => null,
            'parent_task_id' => null,
            'sub_project_template_id' => $this->faker->numberBetween(1, 3)
        ];
    }
}
