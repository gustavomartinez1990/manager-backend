<?php

namespace Database\Factories;

use App\Models\SubProjectTemplate;
use App\Models\SubProjectStatus;
use App\Models\SubProjectType;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SubProjectTemplateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubProjectTemplate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence(3);
        return [
            'name' => $name,
            'description' => $this->faker->paragraph(2, false),
            'type' => $this->faker->numberBetween(SubProjectType::PRODUCT, SubProjectType::SERVICE),
            'status' => $this->faker->numberBetween(SubProjectStatus::DISABLED, SubProjectStatus::ACTIVE),
            'duration' => $this->faker->randomNumber(3, false),
            'slug' => Str::slug($name, '-')
        ];
    }
}
