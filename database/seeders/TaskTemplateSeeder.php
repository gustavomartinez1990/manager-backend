<?php

namespace Database\Seeders;

use App\Models\TaskTemplate;
use App\Models\TaskStatus;
use App\Models\TaskType;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TaskTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TaskTemplate::create([
            'name' => "Comprar dominio",
            'description' => "Comprar dominio de la web",
            'type' => TaskType::NEW_TASK,
            'status' => TaskStatus::TODO,
            'duration' => 2,
            'is_milestone' => true,
            'is_sub_task' => false,
            'order' => 0,
            'slug' => Str::slug('Comprar dominio', '-'),
            'require_task_id' => null,
            'parent_task_id' => null,
            'sub_project_template_id'=> 1,
        ]);
    }
}
