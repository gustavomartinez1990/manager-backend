<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\DepartmentStatus;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
            'name' => 'Redes Sociales',
            'description' => 'Redes Sociales',
            'status' => DepartmentStatus::ACTIVE
        ]);
        
        Department::create([
            'name' => 'Pagina Web',
            'description' => 'Pagina Web',
            'status' => DepartmentStatus::ACTIVE
        ]);
        
        Department::create([
            'name' => 'Posicionamiento',
            'description' => 'Posicionamiento',
            'status' => DepartmentStatus::ACTIVE
        ]);
        
        Department::create([
            'name' => 'Facebook Ads',
            'description' => 'Facebook Ads',
            'status' => DepartmentStatus::ACTIVE
        ]);
    }
}
