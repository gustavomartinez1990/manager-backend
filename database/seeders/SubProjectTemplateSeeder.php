<?php

namespace Database\Seeders;

use App\Models\SubProjectTemplate;
use App\Models\SubProjectStatus;
use App\Models\SubProjectType;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SubProjectTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubProjectTemplate::create([
            'name' => "Página Web",
            'description' => "Página Web a medida",
            'type' => SubProjectType::PRODUCT,
            'status' => SubProjectStatus::ACTIVE,
            'duration' => 56,
            'slug' => Str::slug('Página Web', '-')
        ]);

        SubProjectTemplate::create([
            'name' => "Redes Sociales",
            'description' => "Gestión de Redes Sociales",
            'type' => SubProjectType::PRODUCT,
            'status' => SubProjectStatus::ACTIVE,
            'duration' => 35,
            'slug' => Str::slug('Redes Sociales', '-')
        ]);

        SubProjectTemplate::create([
            'name' => "Posicionamiento",
            'description' => "Posicionamiento web o SEO",
            'type' => SubProjectType::PRODUCT,
            'status' => SubProjectStatus::ACTIVE,
            'duration' => 45,
            'slug' => Str::slug('Posicionamiento', '-')
        ]);
    }
}
