<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin_permissions = [];
        $manager_permissions = [];
        $employee_permissions = [];

        // Users module permissions.
        array_push($super_admin_permissions, Permission::create(['name' => 'create_users']));
        array_push($super_admin_permissions, Permission::create(['name' => 'edit_users']));
        array_push($super_admin_permissions, Permission::create(['name' => 'list_users']));
        array_push($super_admin_permissions, Permission::create(['name' => 'view_users']));
        array_push($super_admin_permissions, Permission::create(['name' => 'delete_users']));
        array_push($super_admin_permissions, Permission::create(['name' => 'change_password']));
        array_push($super_admin_permissions, Permission::create(['name' => 'assign_manager']));
        array_push($super_admin_permissions, Permission::create(['name' => 'assign_departments']));

        // Deparments module permissions.
        array_push($super_admin_permissions, Permission::create(['name' => 'create_departments']));
        array_push($super_admin_permissions, Permission::create(['name' => 'edit_departments']));
        array_push($super_admin_permissions, Permission::create(['name' => 'list_departments']));
        array_push($super_admin_permissions, Permission::create(['name' => 'view_departments']));
        array_push($super_admin_permissions, Permission::create(['name' => 'delete_departments']));

        // Clients module permissions.
        array_push($super_admin_permissions, Permission::create(['name' => 'create_clients']));
        array_push($super_admin_permissions, Permission::create(['name' => 'edit_clients']));
        array_push($super_admin_permissions, Permission::create(['name' => 'list_clients']));
        array_push($super_admin_permissions, Permission::create(['name' => 'view_clients']));
        array_push($super_admin_permissions, Permission::create(['name' => 'delete_clients']));
        array_push($super_admin_permissions, Permission::create(['name' => 'assign_referral_clients']));

        // Projects module permissions.
        array_push($super_admin_permissions, Permission::create(['name' => 'create_projects']));
        array_push($super_admin_permissions, Permission::create(['name' => 'edit_projects']));
        array_push($super_admin_permissions, Permission::create(['name' => 'list_projects']));
        array_push($super_admin_permissions, Permission::create(['name' => 'view_projects']));
        array_push($super_admin_permissions, Permission::create(['name' => 'delete_projects']));

        // Sub Projects module permissions.
        array_push($super_admin_permissions, Permission::create(['name' => 'create_subprojects']));
        array_push($super_admin_permissions, Permission::create(['name' => 'edit_subprojects']));
        array_push($super_admin_permissions, Permission::create(['name' => 'list_subprojects']));
        array_push($super_admin_permissions, Permission::create(['name' => 'view_subprojects']));
        array_push($super_admin_permissions, Permission::create(['name' => 'delete_subprojects']));

        // Template tasks module permissions.
        array_push($super_admin_permissions, Permission::create(['name' => 'create_template_tasks']));
        array_push($super_admin_permissions, Permission::create(['name' => 'edit_template_tasks']));
        array_push($super_admin_permissions, Permission::create(['name' => 'list_template_tasks']));
        array_push($super_admin_permissions, Permission::create(['name' => 'view_template_tasks']));
        array_push($super_admin_permissions, Permission::create(['name' => 'delete_template_tasks']));

        // Project tasks module permissions.
        array_push($super_admin_permissions, Permission::create(['name' => 'create_tasks']));
        array_push($super_admin_permissions, Permission::create(['name' => 'edit_tasks']));
        array_push($super_admin_permissions, Permission::create(['name' => 'list_tasks']));
        array_push($super_admin_permissions, Permission::create(['name' => 'view_tasks']));
        array_push($super_admin_permissions, Permission::create(['name' => 'delete_tasks']));
        array_push($super_admin_permissions, Permission::create(['name' => 'asign_tasks']));

        // Role Super Admin.
        $role = Role::create(['name' => 'super_admin']);
        $role->syncPermissions($super_admin_permissions);

        // Users module permissions.
        array_push($manager_permissions, Permission::create(['name' => 'list_assigned_users']));
        array_push($manager_permissions, Permission::create(['name' => 'view_assigned_users']));
        
        // Project tasks module permissions.
        array_push($manager_permissions, Permission::create(['name' => 'create_subproject_task']));
        array_push($manager_permissions, Permission::create(['name' => 'edit_subproject_task']));
        array_push($manager_permissions, Permission::create(['name' => 'list_subproject_tasks']));
        array_push($manager_permissions, Permission::create(['name' => 'view_subproject_tasks']));
        array_push($manager_permissions, Permission::create(['name' => 'asign_subproject_tasks']));
        array_push($manager_permissions, Permission::create(['name' => 'edit_subproject_task_delivery_date']));
        array_push($manager_permissions, Permission::create(['name' => 'list_assigned_tasks']));
        array_push($manager_permissions, Permission::create(['name' => 'view_assigned_tasks']));
        array_push($manager_permissions, Permission::create(['name' => 'edit_assigned_task']));

        // Role Manager.
        $role = Role::create(['name' => 'manager']);
        $role->syncPermissions($manager_permissions);
        
        // Project tasks module permissions.
        array_push($employee_permissions, 'list_subproject_tasks');
        array_push($employee_permissions, 'view_subproject_tasks');
        array_push($employee_permissions, 'edit_subproject_task_delivery_date');
        array_push($employee_permissions, 'list_assigned_tasks');
        array_push($employee_permissions, 'view_assigned_tasks');

        // Role Employee.
        $role = Role::create(['name' => 'employee']);
        $role->syncPermissions($employee_permissions);


        // Create user and assign super_admin role.
        $userSuperAdminGustavo = User::create([
            'name' => 'Gustavo',
            'email' => 'gustavo@merkadoweb.net',
            'password' => bcrypt('15091976')
        ]);
        $userSuperAdminGustavo->assignRole('super_admin');

        // Create user and assign super_admin role.
        $userSuperAdminRaul = User::create([
            'name' => 'Raul',
            'email' => 'raul@merkadoweb.net',
            'password' => bcrypt('20542072')
        ]);
        $userSuperAdminRaul->assignRole('super_admin');

        // Create user and assign manager role.
        $managerUser = User::create([
            'name' => 'Manager',
            'email' => 'manager@merkadoweb.net',
            'password' => bcrypt('19755968')
        ]);
        $managerUser->assignRole('manager');
    }
}
