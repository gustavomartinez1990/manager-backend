<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\ClientStatus;
use App\Models\ClientType;

use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create([
            'fiscal_name' => "Empresa Fiscal",
            'commercial_name' => "Empresa Comercial",
            'nif' => 'A-81845091',
            'phone_number' => '607340197',
            'email' => 'inogesd@gmail.com',
            'logo_url' => '46f7eef24e63c36b314706580d08a108.png',
            'country' => 'ES',
            'city' => 'Madrid',
            'street_address' => 'Calle Cantabria nº 2, Planta 2ª, Puerta A1',
            'postal_code' => '28046',
            'type' => ClientType::AGENCY,
            'status' => ClientStatus::ACTIVE
        ]);

        
        Client::create([
            'fiscal_name' => "Empresa Fiscal #2",
            'commercial_name' => "Empresa Comercial #2",
            'nif' => 'A-21845093',
            'phone_number' => '607340197',
            'email' => 'miguelangel.campo@bovis.es',
            'logo_url' => '23f6eef24e63c36b314706580d08a601.png',
            'country' => 'ES',
            'city' => 'Madrid',
            'street_address' => 'Paseo de la Castellana 259 - Planta 20 sur Torre de Cristal',
            'postal_code' => '28046',
            'type' => ClientType::CLIENT,
            'status' => ClientStatus::ACTIVE,
            'referral_id' => 1
        ]);
    }
}
