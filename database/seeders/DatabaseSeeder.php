<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // \App\Models\Department::factory(10)->create();
        // \App\Models\Client::factory(10)->create();
        // \App\Models\SubProjectTemplate::factory(10)->create();
        // \App\Models\TaskTemplate::factory(10)->create();
        // \App\Models\Project::factory(10)->create();
        // \App\Models\SubProject::factory(10)->create();

        // Seed needed for the app to run.
        $this->call(PermissionsSeeder::class);
        $this->call(DepartmentSeeder::class);

        // Seed of additional test data for local environment.
        // $this->call(ClientSeeder::class);
        // $this->call(SubProjectTemplateSeeder::class);
        // $this->call(TaskTemplateSeeder::class);
        // $this->call(ProjectSeeder::class);
        // $this->call(SubProjectSeeder::class);
    }
}
