<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\ProjectPriority;
use App\Models\ProjectStatus;
use App\Models\ProjectType;

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::create([
            'name' => "Sample Project #1",
            'description' => "Sample Project #1 description example",
            'documentation_url'=> 'https://drive.google.com/drive/u/0/my-drive',
            'type' => ProjectType::CONSULTANT,
            'status' => ProjectStatus::TODO,
            'priority' => ProjectPriority::MEDIUM,
            'total_sub_projects' => 3,
            'cost' => 180.75,
            'client_id' => 1,
            'assigned_to' => 3
        ]);

        Project::create([
            'name' => "Sample Project #2",
            'description' => "Sample Project #1 description example",
            'type' => ProjectType::CONSULTANT,
            'status' => ProjectStatus::TODO,
            'priority' => ProjectPriority::HIGH,
            'total_sub_projects' => 3,
            'cost' => 300.75,
            'client_id' => 2,
            'assigned_to' => 3
        ]);
    }
}
