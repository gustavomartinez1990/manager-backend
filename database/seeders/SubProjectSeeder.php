<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\SubProject;

use App\Models\SubProjectPriority;
use App\Models\SubProjectStatus;
use App\Models\SubProjectType;

use Carbon\CarbonImmutable;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SubProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project = Project::first();
        
        $startDate = CarbonImmutable::now()->add(1, 'day');
        $dueDate = CarbonImmutable::now()->add(15, 'day');
        
        SubProject::create([
            'name' => "Página Web",
            'description' => "Página Web a medida",
            'slug' => Str::slug('Página Web', '-'),
            'type' => SubProjectType::PRODUCT,
            'status' => SubProjectStatus::ACTIVE,
            'priority' => SubProjectPriority::HIGH,
            'duration' => 0,
            'remaining_duration' => 0,
            'total_tasks' => 0,
            'remaining_tasks' => 0,
            'cost' => 100.75,
            'client_id' => $project->id,
            'project_id' => $project->client_id,
            'assigned_to' => 3,
            'due_date' => $dueDate,
            'start_date' => $startDate,
        ]);
        
        SubProject::create([
            'name' => "Redes Sociales",
            'description' => "Gestión de Redes Sociales",
            'slug' => Str::slug('Redes Sociales', '-'),
            'type' => SubProjectType::PRODUCT,
            'status' => SubProjectStatus::ACTIVE,
            'priority' => SubProjectPriority::MEDIUM,
            'duration' => 0,
            'remaining_duration' => 0,
            'total_tasks' => 0,
            'remaining_tasks' => 0,
            'cost' => 40,
            'client_id' => $project->id,
            'project_id' => $project->client_id,
            'assigned_to' => 3,
            'due_date' => $dueDate,
            'start_date' => $startDate,
        ]);
        
        SubProject::create([
            'name' => "Posicionamiento",
            'description' => "Posicionamiento web o SEO",
            'slug' => Str::slug('Posicionamiento', '-'),
            'type' => SubProjectType::PRODUCT,
            'status' => SubProjectStatus::ACTIVE,
            'priority' => SubProjectPriority::LOW,
            'duration' => 0,
            'remaining_duration' => 0,
            'total_tasks' => 0,
            'remaining_tasks' => 0,
            'cost' => 40,
            'client_id' => $project->id,
            'project_id' => $project->client_id,
            'assigned_to' => 3,
            'due_date' => $dueDate,
            'start_date' => $startDate,
        ]);
    }
}
