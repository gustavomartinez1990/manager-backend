<?php

namespace App\Models;

class SubProjectPriority
{
    const LOWEST = 0;
    const LOW = 1;
    const MEDIUM = 2;
    const HIGH = 3;
    const HIGHEST = 4;
    
    public static function listPriorities() {
        return [
            SubProjectPriority::LOWEST,
            SubProjectPriority::LOW,
            SubProjectPriority::MEDIUM,
            SubProjectPriority::HIGH,
            SubProjectPriority::HIGHEST,
        ];
    }

    public static function listPrioritiesValue() {
        return [
            SubProjectPriority::LOWEST => 'Muy baja',
            SubProjectPriority::LOW => 'Baja',
            SubProjectPriority::MEDIUM => 'Media',
            SubProjectPriority::HIGH => 'Alta',
            SubProjectPriority::HIGHEST => 'Urgente',
        ];
    }
}
