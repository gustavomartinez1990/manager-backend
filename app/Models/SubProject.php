<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class SubProject extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'slug',
        'type',
        'status',
        'priority',
        'duration',
        'remaining_duration',
        'total_tasks',
        'remaining_tasks',
        'cost',
        'client_id',
        'project_id',
        'assigned_to',
        'due_date',
        'start_date',
        'started_at',
        'finished_at'
    ];
    
    // Relation with Client.
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    
    // Relation with Project.
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    // Relation with User.
    public function assignedTo()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Creates a SubProject from a SubProjectTemplate.
     * Creates a Task for each TemplateTask of the SubProjectTemplate.
     * Update SubProject's Project cost.
     * Update SubProject's Project total_sub_projects.
     */
    public function handleCreateSubProject($arguments, $sub_project_template)
    {
        $arguments["slug"] = Str::slug($arguments["name"], '-');
        $arguments["remaining_duration"] = $arguments["duration"];
        $arguments["remaining_tasks"] = $arguments["total_tasks"];
        $subProject = $this->create($arguments);

        // TODO: Move this logic to a event.
        $project = $subProject->project;
        $project->cost = $project->cost + $arguments["cost"];
        $project->total_sub_projects = $project->total_sub_projects + 1;
        $project->save();

        // Create all Tasks of SubProject from SubProjectTemplate Tasks.
        $taskTemplates = TaskTemplate::where('sub_project_template_id', $sub_project_template)->orderBy('id','asc')->get();
        foreach ($taskTemplates as $task) {
            $require_task_id = null;
            $parent_task_id = null;
            // Get require_task_id or parent_task_id from Task table.
            if(isset($task->require_task_id) && ($task->require_task_id != null)) {
                $requireTask = Task::where('sub_project_id', $subProject->id)->where('task_template_id', $task->require_task_id)->first();
                $require_task_id = $requireTask->id;
            }
            if(isset($task->parent_task_id) && ($task->parent_task_id != null)) {
                $parentTask = Task::where('task_template_id', $task->parent_task_id)->where('sub_project_id ', $subProject->id)->first();
                $parent_task_id = $parentTask->id;
            }

            Task::create([
                "name" => $task->name,
                'description' => $task->description,
                'priority' => TaskPriority::MEDIUM,
                'type' => $task->type,
                'status' => $task->status,
                'duration' => $task->duration,
                'is_milestone' => $task->is_milestone,
                'is_sub_task' => $task->is_sub_task,
                'order' => $task->order,
                'slug' => $task->slug,
                'assigned_to' => $subProject->assigned_to,
                'due_date' => $subProject->due_date,
                'require_task_id' => $require_task_id,
                'parent_task_id' => $parent_task_id,
                'sub_project_id' => $subProject->id,
                'task_template_id' => $task->id
            ]);
        }

        return $subProject;
    }

    /**
     * Updates a SubProject data.
     * Update SubProject's Project cost.
     * Update SubProject's Project total_sub_projects.
     */
    public function handleUpdateSubProject($arguments)
    {
        $arguments["slug"] = Str::slug($arguments["name"], '-');

        // TODO: Search if this logic can be moved to an afterSave.
        // Refresh project cost.
        if(isset($arguments["cost"]) && ($arguments["cost"]!=$this->cost)) {
            $oldCost = $this->cost;
            $project = $this->project;

            $project->cost = ($project->cost - $oldCost) + $arguments["cost"];
            $project->save();
        }

        // Update Sub Project information.
        $this->update($arguments);
    }
}
