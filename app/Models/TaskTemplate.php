<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskTemplate extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'type',
        'status',
        'duration',
        'is_milestone',
        'is_sub_task',
        'order',
        'slug',
        'require_task_id',
        'parent_task_id',
        'sub_project_template_id'
    ];
    
    // Relation with TaskTemplate
    public function requireTask()
    {
        return $this->belongsTo(TaskTemplate::class);
    }

    // Relation with TaskTemplate
    public function parentTask()
    {
        return $this->belongsTo(TaskTemplate::class);
    }

    // Relation with SubProjectTemplate
    public function subProjectTemplate()
    {
        return $this->belongsTo(SubProjectTemplate::class);
    }
}
