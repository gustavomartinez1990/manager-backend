<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'priority',
        'type',
        'status',
        'duration',
        'is_milestone',
        'is_sub_task',
        'order',
        'slug',
        'due_date',
        'started_at',
        'finished_at',
        'require_task_id',
        'parent_task_id',
        'sub_project_id',
        'assigned_to',
        'task_template_id',
    ];

    // Relation with User.
    public function assignedTo()
    {
        return $this->belongsTo(User::class);
    }

    // Relation with Task
    public function requireTask()
    {
        return $this->belongsTo(Task::class);
    }

    // Relation with Task
    public function parentTask()
    {
        return $this->belongsTo(Task::class);
    }

    // Relation with SubProject
    public function subProject()
    {
        return $this->belongsTo(SubProject::class);
    }

    /**
     * Creates a new Task.
     * Update Task's SubProject total_tasks.
     * Update Task's SubProject duration.
     * Update Task's SubProject remaining_duration.
     * Update Task's SubProject remaining_tasks.
     */
    public function handleCreateTask($arguments)
    {
        $arguments["slug"] = Str::slug($arguments["name"], '-');
        $task = $this->create($arguments);

        // Update Subproject Data.
        $subProject = $task->subProject;
        $subProject->total_tasks = $subProject->total_tasks +1;
        $subProject->duration = $subProject->duration + $task->duration;
        $subProject->remaining_duration = $subProject->remaining_duration + $task->duration;
        $subProject->remaining_tasks = $subProject->remaining_tasks + 1;
        $subProject->save();

        return $task;
    }

    /**
     * Update a Task.
     * Update Task's SubProject total_tasks.
     * Update Task's SubProject duration.
     * Update Task's SubProject remaining_duration.
     * Update Task's SubProject remaining_tasks.
     */
    public function handleUpdateTask($arguments)
    {
        $arguments["slug"] = Str::slug($arguments["name"], '-');

        if($arguments["duration"]!=$this->duration) {
            // TODO: Search if this logic can be moved to an afterSave.
            // Update Subproject Data.
            $subProject = $task->subProject;
            $subProject->duration = ($subProject->duration - $task->duration) + $arguments["duration"];
            $subProject->remaining_duration = ($subProject->remaining_duration - $task->duration) + $arguments["duration"];
            $subProject->save();
        }

        $this->update($arguments);
    }
}
