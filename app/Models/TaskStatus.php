<?php

namespace App\Models;

class TaskStatus
{
    const DISABLED = 0;
    const ACTIVE = 1;
    const TODO = 2;
    const DOING = 3;
    const TEST = 4;
    const REWORK = 5;
    const DONE = 6;

    public static function listStatus() {
        return [
            TaskStatus::DISABLED,
            TaskStatus::ACTIVE,
            TaskStatus::TODO,
            TaskStatus::DOING,
            TaskStatus::TEST,
            TaskStatus::REWORK,
            TaskStatus::DONE,
        ];
    }

    public static function listStatusValue() {
        return [
            TaskStatus::DISABLED => 'Deshabilitado',
            TaskStatus::ACTIVE => 'Activo',
            TaskStatus::TODO => 'Por hacer',
            TaskStatus::DOING => 'En proceso',
            TaskStatus::TEST => 'Pruebas',
            TaskStatus::REWORK => 'Necesita mejoras',
            TaskStatus::DONE => 'Finalizado',
        ];
    }
}
