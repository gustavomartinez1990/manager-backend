<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'fiscal_name',
        'commercial_name',
        'nif',
        'phone_number',
        'email',
        'logo_url',
        'country',
        'city',
        'street_address',
        'postal_code',
        'type',
        'status',
        'referral_id'
    ];
}
