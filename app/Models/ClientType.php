<?php

namespace App\Models;

class ClientType
{
    const CLIENT = 0;
    const AGENCY = 1;
    const RESELLER = 2;

    public static function listTypesValue() {
        return [
            ClientType::CLIENT => 'Cliente',
            ClientType::AGENCY => 'Agencia',
            ClientType::RESELLER => 'Revendedor',
        ];
    }
    

    public static function listTypes() {
        return [
            ClientType::CLIENT,
            ClientType::AGENCY,
            ClientType::RESELLER,
        ];
    }
}
