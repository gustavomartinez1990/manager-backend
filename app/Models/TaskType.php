<?php

namespace App\Models;

class TaskType
{
    const NEW_TASK = 0;
    const NEW_AND_CYCLIC = 1;
    const CYCLIC_AFTER_FINISH = 2;
    const FIX = 3;
    const ADDITIONAL_FEATURE = 4;
    const MAINTENANCE = 5;

    public static function listTypesValue() {
        return [
            TaskType::NEW_TASK => 'Nueva tarea',
            TaskType::NEW_AND_CYCLIC => 'Nueva tarea y repetitiva',
            TaskType::CYCLIC_AFTER_FINISH => 'Tarea repetitiva posterior a terminar el proyecto',
            TaskType::FIX => 'Reparación',
            TaskType::ADDITIONAL_FEATURE => 'Modificación',
            TaskType::MAINTENANCE => 'Ticket de soporte',
        ];
    }

    public static function listTypes() {
        return [
            TaskType::NEW_TASK,
            TaskType::NEW_AND_CYCLIC,
            TaskType::CYCLIC_AFTER_FINISH,
            TaskType::FIX,
            TaskType::ADDITIONAL_FEATURE,
            TaskType::MAINTENANCE
        ];
    }
}
