<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'documentation_url',
        'type',
        'status',
        'priority',
        'total_sub_projects',
        'cost',
        'client_id',
        'assigned_to'
    ];
    
    // Relation with Client.
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    // Relation with User.
    public function assignedTo()
    {
        return $this->belongsTo(User::class);
    }
}
