<?php

namespace App\Models;

class TaskPriority
{
    const LOWEST = 0;
    const LOW = 1;
    const MEDIUM = 2;
    const HIGH = 3;
    const HIGHEST = 4;
    
    public static function listPriorities() {
        return [
            TaskPriority::LOWEST,
            TaskPriority::LOW,
            TaskPriority::MEDIUM,
            TaskPriority::HIGH,
            TaskPriority::HIGHEST,
        ];
    }

    public static function listPrioritiesValue() {
        return [
            TaskPriority::LOWEST => 'Muy baja',
            TaskPriority::LOW => 'Baja',
            TaskPriority::MEDIUM => 'Media',
            TaskPriority::HIGH => 'Alta',
            TaskPriority::HIGHEST => 'Urgente',
        ];
    }
}
