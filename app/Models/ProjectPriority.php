<?php

namespace App\Models;

class ProjectPriority
{
    const LOWEST = 0;
    const LOW = 1;
    const MEDIUM = 2;
    const HIGH = 3;
    const HIGHEST = 4;
    
    public static function listPriorities() {
        return [
            ProjectPriority::LOWEST,
            ProjectPriority::LOW,
            ProjectPriority::MEDIUM,
            ProjectPriority::HIGH,
            ProjectPriority::HIGHEST,
        ];
    }

    public static function listPrioritiesValue() {
        return [
            ProjectPriority::LOWEST => 'Muy baja',
            ProjectPriority::LOW => 'Baja',
            ProjectPriority::MEDIUM => 'Media',
            ProjectPriority::HIGH => 'Alta',
            ProjectPriority::HIGHEST => 'Urgente',
        ];
    }
}
