<?php

namespace App\Models;

class ClientStatus
{
    const DISABLED = 0;
    const ACTIVE = 1;
    const DELETED = 2;

    public static function listStatus() {
        return [
            ClientStatus::DISABLED,
            ClientStatus::ACTIVE,
            ClientStatus::DELETED,
        ];
    }

    public static function listStatusValue() {
        return [
            ClientStatus::DISABLED => 'Deshabilitado',
            ClientStatus::ACTIVE => 'Activo',
            ClientStatus::DELETED => 'Borrado',
        ];
    }
}
