<?php

namespace App\Models;

class SubProjectStatus
{
    const DISABLED = 0;
    const ACTIVE = 1;

    public static function listStatus() {
        return [
            SubProjectStatus::DISABLED,
            SubProjectStatus::ACTIVE,
        ];
    }

    public static function listStatusValue() {
        return [
            SubProjectStatus::DISABLED => 'Deshabilitado',
            SubProjectStatus::ACTIVE => 'Activo',
        ];
    }
}
