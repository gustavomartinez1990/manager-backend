<?php

namespace App\Models;

class DepartmentStatus
{
    const DISABLED = 0;
    const ACTIVE = 1;

    public static function listStatus() {
        return [
            DepartmentStatus::DISABLED,
            DepartmentStatus::ACTIVE,
        ];
    }

    public static function listStatusValue() {
        return [
            DepartmentStatus::DISABLED => 'Deshabilitado',
            DepartmentStatus::ACTIVE => 'Activo',
        ];
    }
}
