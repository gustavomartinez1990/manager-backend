<?php

namespace App\Models;

class ProjectType
{
    const CONSULTANT = 0;

    public static function listTypes() {
        return [
            ProjectType::CONSULTANT,
        ];
    }

    public static function listTypesValue() {
        return [
            ProjectType::CONSULTANT => 'Consultoría',
        ];
    }
}
