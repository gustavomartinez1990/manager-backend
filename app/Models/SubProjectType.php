<?php

namespace App\Models;

class SubProjectType
{
    const PRODUCT = 0;
    const SERVICE = 1;

    public static function listTypesValue() {
        return [
            SubProjectType::PRODUCT => 'Producto',
            SubProjectType::SERVICE => 'Servicio',
        ];
    }
    

    public static function listTypes() {
        return [
            SubProjectType::PRODUCT,
            SubProjectType::SERVICE,
        ];
    }
}
