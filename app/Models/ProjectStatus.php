<?php

namespace App\Models;

class ProjectStatus
{
    const DISABLED = 0;
    const ACTIVE = 1;
    const TODO = 2;

    public static function listStatus() {
        return [
            ProjectStatus::DISABLED,
            ProjectStatus::ACTIVE,
            ProjectStatus::TODO,
        ];
    }

    public static function listStatusValue() {
        return [
            ProjectStatus::DISABLED => 'Deshabilitado',
            ProjectStatus::ACTIVE => 'Activo',
            ProjectStatus::TODO => 'Por hacer',
        ];
    }
}
