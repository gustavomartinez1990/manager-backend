<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SubProject;
use App\Models\Task;
use App\Models\TaskStatus;
use App\Models\Client;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function stats()
    {
        $clients = Client::orderBy('id','desc')->get();
        $subProjects = SubProject::orderBy('id','desc')->get();
        $pendingTasks = Task::where('status','!=',TaskStatus::DONE)->get();
        $finishedTasks = Task::where('status',TaskStatus::DONE)->get();
        return response()->json(            
            [
                'clients' => $clients->count(),
                'subProjects' => $subProjects->count(),
                'pendingTasks' => $pendingTasks->count(),
                'finishedTasks' => $finishedTasks->count(),
            ], 200
        );
    }
}
