<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TaskTemplate;
use App\Models\SubProjectTemplate;

use App\Http\Requests\TaskTemplate as TaskTemplateRequests;
use App\Http\Resources\TaskTemplate as TaskTemplateResources;
use App\Http\Resources\TaskTemplateCollection;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TaskTemplateController extends Controller
{
    protected $taskTemplate;

    public function __construct(TaskTemplate $taskTemplate)
    {
        $this->taskTemplate = $taskTemplate;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            new TaskTemplateCollection(
                $this->taskTemplate->orderBy('id','desc')->get()
            )
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subProjectTemplateTasks(SubProjectTemplate $subProjectTemplate)
    {
        return response()->json(
            new TaskTemplateCollection(
                $this->taskTemplate->where('sub_project_template_id','=', $subProjectTemplate->id)->orderBy('id','desc')->get()
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskTemplateRequests $request)
    {
        $inputData = $request->all();
        $inputData["slug"] = Str::slug($inputData["name"], '-');
        $taskTemplate = $this->taskTemplate->create($inputData);

        $subProjectTemplate = $taskTemplate->subProjectTemplate;
        $subProjectTemplate->total_tasks = $subProjectTemplate->total_tasks +1;
        $subProjectTemplate->duration = $subProjectTemplate->duration + $taskTemplate->duration;
        $subProjectTemplate->save();

        return response()->json(
            new TaskTemplateResources($taskTemplate), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TaskTemplate  $taskTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(TaskTemplate $taskTemplate)
    {
        return response()->json(
            new TaskTemplateResources($taskTemplate)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TaskTemplate  $taskTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(TaskTemplateRequests $request, TaskTemplate $taskTemplate)
    {
        $inputData = $request->all();
        $inputData["slug"] = Str::slug($inputData["name"], '-');

        if($inputData["duration"]!=$taskTemplate->duration) {
            // TODO: Search if this logic can be moved to an afterSave.
            $subProjectTemplate = $taskTemplate->subProjectTemplate;
            $subProjectTemplate->duration = ($subProjectTemplate->duration - $taskTemplate->duration) + $inputData["duration"];
            $subProjectTemplate->save();
        }

        $taskTemplate->update($inputData);

        return response()->json(
            new TaskTemplateResources($taskTemplate)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TaskTemplate  $taskTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaskTemplate $taskTemplate)
    {
        $subProjectTemplate = $taskTemplate->subProjectTemplate;
        $duration = $taskTemplate->duration;
        $taskTemplate->delete();

        $subProjectTemplate->total_tasks = $subProjectTemplate->total_tasks -1;
        $subProjectTemplate->duration = $subProjectTemplate->duration - $duration;
        $subProjectTemplate->save();
        
        return response()->json(null, 204);
    }
}
