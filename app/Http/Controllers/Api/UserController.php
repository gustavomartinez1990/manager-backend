<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests\User as UserRequests;
use App\Http\Resources\User as UserResources;
use App\Http\Resources\UserCollection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Example of how to retrieve the currently authenticated user.
        // And how to check for permissions.
        // if ($request->user()->cannot('list_users')) {
        //     return response()->json(["message"=>"User doesn't have enough privilege"], 403);
        // }

        return response()->json(
            new UserCollection(
                $this->user->orderBy('id','desc')->get()
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequests $request)
    {
        $inputs = $request->all();
        $inputs["password"] = bcrypt($inputs["password"]);
        $user = $this->user->create($inputs);
        $user->assignRole($inputs['role']);

        return response()->json(new UserResources($user), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return response()->json(new UserResources($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $inputs = $request->all();

        $validator = Validator::make(
            $request->all(),
            [ 
                'name' => 'required',
                'email' => [
                    'required',
                    Rule::unique('users')->ignore($user)
                ],
                'role' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 422);
        }
        $user->update($inputs);
        




        // Change role if needed.
        $roles = $user->getRoleNames();
        if($roles[0] !== $inputs['role']) {
            $user->removeRole($roles[0]);
            $user->assignRole($inputs['role']);
        }
    
        return response()->json(new UserResources($user));
    }

    

    /**
     * Update the specified resource password in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request, User $user)
    {
        $inputs = $request->all();
        $validator = Validator::make(
            $request->all(), [ 
                'password' => 'required',
            ]
        );

        $inputs["password"] = bcrypt($inputs["password"]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 422);
        }

        $user->update($inputs);
    
        return response()->json(new UserResources($user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(null, 204);
    }
}
