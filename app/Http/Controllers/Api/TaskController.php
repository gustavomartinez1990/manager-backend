<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\SubProject;
use App\Models\Task;
use App\Models\TaskStatus;


use App\Http\Requests\Task as TaskRequests;
use App\Http\Resources\Task as TaskResources;
use App\Http\Resources\TaskCollection;

use Carbon\Carbon;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($subProject)
    {
        $toDo = $this->task->where('sub_project_id',$subProject)
            ->where('status',TaskStatus::TODO)->orderBy('id','desc')->get();
        $doing = $this->task->where('sub_project_id',$subProject)
            ->where('status',TaskStatus::DOING)->orderBy('id','desc')->get();
        $test = $this->task->where('sub_project_id',$subProject)
            ->where('status',TaskStatus::TEST)->orderBy('id','desc')->get();
        $done = $this->task->where('sub_project_id',$subProject)
            ->where('status',TaskStatus::DONE)->orderBy('id','desc')->get();

        return response()->json(
            [
                'lists' => [
                    (string) TaskStatus::TODO => [
                        'id' => (string) TaskStatus::TODO,
                        'title' => 'Por Iniciar',
                        'cards' => $toDo,
                    ],
                    (string) TaskStatus::DOING => [
                        'id' => (string) TaskStatus::DOING,
                        'title' => 'En proceso',
                        'cards' => $doing,
                    ],
                    (string) TaskStatus::TEST => [
                        'id' => (string) TaskStatus::TEST,
                        'title' => 'Por revisión',
                        'cards' => $test,
                    ],
                    (string) TaskStatus::DONE => [
                        'id' => (string) TaskStatus::DONE,
                        'title' => 'Finalizado',
                        'cards' => $done,
                    ],
                ],
                'listIds' => [
                    (string) TaskStatus::TODO, 
                    (string) TaskStatus::DOING, 
                    (string) TaskStatus::TEST, 
                    (string) TaskStatus::DONE,
                ],
            ], 200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequests $request)
    {
        $inputData = $request->all();
        $task = $this->task->handleCreateTask($inputData);

        return response()->json(
            new TaskResources($task), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return response()->json(
            new TaskResources($task)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(TaskRequests $request, Task $task)
    {
        // Update Task using handleUpdateTask.
        $task->handleUpdateTask($request->all());

        return response()->json(
            new TaskResources($task)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request, Task $task)
    {
        $input = $request->all();
        if($input['status'] !=  $task->status) {
            $oldStatus = $task->status;
            $task->status = $input['status'];
            $task->save();
            // Update SubProject data if task is DONE.
            if ($input['status'] === TaskStatus::DONE) {
                $subProject = $task->subProject;
                $subProject->remaining_duration = $subProject->remaining_duration - $task->duration;
                $subProject->remaining_tasks = $subProject->remaining_tasks - 1;

                $subProject->save();
            }
            // Update SubProject data if task was DONE and now status changed.
            if (($input['status'] !== TaskStatus::DONE) && ($oldStatus === TaskStatus::DONE)) {
                $subProject = $task->subProject;
                $subProject->remaining_duration = $subProject->remaining_duration + $task->duration;
                $subProject->remaining_tasks = $subProject->remaining_tasks + 1;
                $subProject->save();
            }
            
            // Set SubProject as started when one task is touched.
            if ($task->subProject->started_at === null) {
                $subProject = $task->subProject;
                $subProject->started_at = Carbon::now();
                $subProject->save();
            }
            
            // Set SubProject as finished when all tasks are DONE.
            if ($task->subProject->remaining_tasks === 0 && $task->subProject->remaining_duration === 0 && $task->subProject->finished_at === null) {
                $subProject = $task->subProject;
                $subProject->finished_at = Carbon::now();
                $subProject->save();
            }
        }

        return response()->json(
            new TaskResources($task)
        );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        // Update Subproject Data.
        $subProject = $task->subProject;
        $subProject->total_tasks = $subProject->total_tasks - 1;
        $subProject->duration = $subProject->duration - $task->duration;
        $subProject->remaining_duration = $subProject->remaining_duration - $task->duration;
        $subProject->remaining_tasks = $subProject->remaining_tasks - 1;
        $subProject->save();

        $task->delete();

        return response()->json(null, 204);
    }
}
