<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\SubProjectTemplate;

use App\Http\Requests\SubProjectTemplate as SubProjectTemplateRequests;
use App\Http\Resources\SubProjectTemplate as SubProjectTemplateResources;
use App\Http\Resources\SubProjectTemplateCollection;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SubProjectTemplateController extends Controller
{
    protected $subProjectTemplate;

    public function __construct(SubProjectTemplate $subProjectTemplate)
    {
        $this->subProjectTemplate = $subProjectTemplate;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            new SubProjectTemplateCollection(
                $this->subProjectTemplate->orderBy('id','desc')->get()
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubProjectTemplateRequests $request)
    {
        $inputData = $request->all();
        $inputData["slug"] = Str::slug($inputData["name"], '-');
        $subProjectTemplate = $this->subProjectTemplate->create($inputData);

        return response()->json(
            new SubProjectTemplateResources($subProjectTemplate), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubProjectTemplate  $subProjectTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(SubProjectTemplate $subProjectTemplate)
    {
        return response()->json(
            new SubProjectTemplateResources($subProjectTemplate)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubProjectTemplate  $subProjectTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(SubProjectTemplateRequests $request, SubProjectTemplate $subProjectTemplate)
    {
        $inputData = $request->all();
        $inputData["slug"] = Str::slug($inputData["name"], '-');
        $subProjectTemplate->update($inputData);

        return response()->json(
            new SubProjectTemplateResources($subProjectTemplate)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubProjectTemplate  $subProjectTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubProjectTemplate $subProjectTemplate)
    {
        $subProjectTemplate->delete();
        
        return response()->json(null, 204);
    }
}
