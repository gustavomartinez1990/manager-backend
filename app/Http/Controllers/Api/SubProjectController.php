<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\SubProject;
use App\Models\Project;

use App\Http\Requests\SubProject as SubProjectRequests;
use App\Http\Resources\SubProject as SubProjectResources;
use App\Http\Resources\SubProjectCollection;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SubProjectController extends Controller
{
    protected $subProject;

    public function __construct(SubProject $subProject)
    {
        $this->subProject = $subProject;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            new SubProjectCollection(
                $this->subProject->orderBy('id','desc')->get()
            )
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function projectSubProjects(Project $project)
    {
        return response()->json(
            new SubProjectCollection(
                $this->subProject->where('project_id','=', $project->id)->orderBy('id','desc')->get()
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubProjectRequests $request)
    {
        $inputData = $request->all();
        // Create Sub Project using handleCreateSubProject.
        $subProject = $this->subProject->handleCreateSubProject($inputData, $inputData["sup_project_template_id"]);

        return response()->json(
            new SubProjectResources($subProject), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubProject  $subProject
     * @return \Illuminate\Http\Response
     */
    public function show(SubProject $subProject)
    {
        return response()->json(
            new SubProjectResources($subProject)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubProject  $subProject
     * @return \Illuminate\Http\Response
     */
    public function update(SubProjectRequests $request, SubProject $subProject)
    {
        // Update Sub Project using handleUpdateSubProject.
        $subProject->handleUpdateSubProject($request->all());

        return response()->json(
            new SubProjectResources($subProject)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubProject  $subProject
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubProject $subProject)
    {
        $project = $subProject->project;
        $cost = $subProject->cost;
        $subProject->delete();

        $project->total_sub_projects = $project->total_sub_projects - 1;
        $project->cost = $project->cost - $cost;
        $project->save();

        return response()->json(null, 204);
    }
}
