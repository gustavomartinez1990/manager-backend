<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\TaskPriority;
use App\Models\TaskStatus;
use App\Models\TaskType;

class Task extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $requireTask = $this->requireTask !== null ? [
            'id' => $this->requireTask->id,
            'name' => $this->requireTask->name,
        ] : null;

        $parentTask = $this->parentTask !== null ? [
            'id' => $this->parentTask->id,
            'name' => $this->parentTask->name,
        ] : null;

        $subProject = $this->subProject !== null ? [
            'id' => $this->subProject->id,
            'name' => $this->subProject->name,
        ] : null;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'priority' => [
                'code' => $this->priority,
                'value' => TaskPriority::listPrioritiesValue()[$this->priority]
            ],
            'type' => [
                'code' => $this->type,
                'value' => TaskType::listTypesValue()[$this->type]
            ],
            'status' => [
                'code' => $this->status,
                'value' => TaskStatus::listStatusValue()[$this->status]
            ],
            'duration' => $this->duration,
            'is_milestone' => $this->is_milestone,
            'is_sub_task' => $this->is_sub_task,
            'order' => $this->order,
            'slug' => $this->slug,
            'require_task_id' => $requireTask,
            'parent_task_id' => $parentTask,
            'sub_project_id' => $subProject,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}
