<?php

namespace App\Http\Resources;

use App\Models\DepartmentStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class Department extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // TODO: Check if its better to send just status number.
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'status' => [
                'code' => $this->status,
                'value' => DepartmentStatus::listStatusValue()[$this->status]
            ],
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}
