<?php

namespace App\Http\Resources;

use App\Models\SubProjectStatus;
use App\Models\SubProjectType;

use Illuminate\Http\Resources\Json\JsonResource;

class SubProjectTemplate extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'type' => [
                'code' => $this->type,
                'value' => SubProjectType::listTypesValue()[$this->type]
            ],
            'status' => [
                'code' => $this->status,
                'value' => SubProjectStatus::listStatusValue()[$this->status]
            ],
            'duration' => $this->duration,
            'slug' => $this->slug,
            'total_tasks' => $this->total_tasks,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}
