<?php

namespace App\Http\Resources;

use App\Models\ClientStatus;
use App\Models\ClientType;
use Illuminate\Http\Resources\Json\JsonResource;

class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fiscal_name' => $this->fiscal_name,
            'commercial_name' => $this->commercial_name,
            'nif' => $this->nif,
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'logo_url' => $this->logo_url, //Transform URL
            'country'=> $this->country,
            'city' => $this->city,
            'street_address' => $this->street_address,
            'postal_code' => $this->postal_code,
            'type' => [
                'code' => $this->type,
                'value' => ClientType::listTypesValue()[$this->type]
            ],
            'status' => [
                'code' => $this->status,
                'value' => ClientStatus::listStatusValue()[$this->status]
            ],
            'referral_id' => $this->referral_id,
        ];
    }
}
