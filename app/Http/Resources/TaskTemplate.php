<?php

namespace App\Http\Resources;

use App\Models\TaskStatus;
use App\Models\TaskType;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskTemplate extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $requireTask = $this->requireTask !== null ? [
            'id' => $this->requireTask->id,
            'name' => $this->requireTask->name,
        ] : null;

        $parentTask = $this->parentTask !== null ? [
            'id' => $this->parentTask->id,
            'name' => $this->parentTask->name,
        ] : null;

        $subProjectTemplate = $this->subProjectTemplate !== null ? [
            'id' => $this->subProjectTemplate->id,
            'name' => $this->subProjectTemplate->name,
        ] : null;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'type' => [
                'code' => $this->type,
                'value' => TaskType::listTypesValue()[$this->type]
            ],
            'status' => [
                'code' => $this->status,
                'value' => TaskStatus::listStatusValue()[$this->status]
            ],
            'duration' => $this->duration,
            'is_milestone' => $this->is_milestone,
            'order' => $this->order,
            'slug' => $this->slug,
            'require_task_id' => $requireTask,
            'parent_task_id' => $parentTask,
            'sub_project_template_id' => $subProjectTemplate,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}
