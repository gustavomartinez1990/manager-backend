<?php

namespace App\Http\Resources;
use App\Models\SubProjectPriority;
use App\Models\SubProjectStatus;
use App\Models\SubProjectType;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SubProject extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $client = $this->client !== null ? [
            'id' => $this->client->id,
            'fiscal_name' => $this->client->fiscal_name,
        ] : null;

        $project = $this->project !== null ? [
            'id' => $this->project->id,
            'fiscal_name' => $this->project->name,
        ] : null;

        $assignedTo = $this->assignedTo !== null ? [
            'id' => $this->assignedTo->id,
            'name' => $this->assignedTo->name,
        ] : null;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'type' => [
                'code' => $this->type,
                'value' => SubProjectType::listTypesValue()[$this->type]
            ],
            'status' => [
                'code' => $this->status,
                'value' => SubProjectStatus::listStatusValue()[$this->status]
            ],
            'priority' => [
                'code' => $this->priority,
                'value' => SubProjectPriority::listPrioritiesValue()[$this->priority]
            ],
            'duration' => $this->duration,
            'remaining_duration' => $this->remaining_duration,
            'total_tasks' => $this->total_tasks,
            'remaining_tasks' => $this->remaining_tasks,
            'cost' => $this->cost, 
            'client_id' => $client,
            'project_id' => $project,
            'assigned_to' => $assignedTo,
            'due_date' => $this->due_date,
            'start_date' => $this->start_date,
            'created_at' => $this->created_at,
        ];
    }
}
