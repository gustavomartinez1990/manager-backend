<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // User permissions.
        $permissions = json_decode(json_encode( $this->getAllPermissions()), true);
        $userPermissions = [];
        foreach ($permissions as $key => $value) {
            $permission= [];
            $permission = array_intersect_key($value, /* main array*/
                array_flip( /* to be extracted */
                    array('name')
                )
            );
            array_push($userPermissions, $permission['name']);
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => count($this->getRoleNames()) > 0 ? $this->getRoleNames()[0] : $this->getRoleNames(),
            'permissions' => $userPermissions,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}
