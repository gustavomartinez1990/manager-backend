<?php

namespace App\Http\Resources;

use App\Models\ProjectPriority;
use App\Models\ProjectStatus;
use App\Models\ProjectType;

use Illuminate\Http\Resources\Json\JsonResource;

class Project extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $client = $this->client !== null ? [
            'id' => $this->client->id,
            'fiscal_name' => $this->client->fiscal_name,
        ] : null;

        $assignedTo = $this->assignedTo !== null ? [
            'id' => $this->assignedTo->id,
            'name' => $this->assignedTo->name,
        ] : null;
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'documentation_url' => $this->documentation_url,
            'type' => [
                'code' => $this->type,
                'value' => ProjectType::listTypesValue()[$this->type]
            ],
            'status' => [
                'code' => $this->status,
                'value' => ProjectStatus::listStatusValue()[$this->status]
            ],
            'priority' => [
                'code' => $this->priority,
                'value' => ProjectPriority::listPrioritiesValue()[$this->priority]
            ],
            'total_sub_projects' => $this->total_sub_projects,
            'cost' => $this->cost,
            'client_id' => $client,
            'assigned_to' => $assignedTo,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}
