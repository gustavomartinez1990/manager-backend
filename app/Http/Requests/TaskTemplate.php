<?php

namespace App\Http\Requests;

use App\Models\TaskStatus;
use App\Models\TaskType;

use Illuminate\Foundation\Http\FormRequest;

class TaskTemplate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $status = TaskStatus::listStatus();
        $types = TaskType::listTypes();

        return [
            'name' => 'required|max:255',
            'description' => 'max:10000',
            'type' => "numeric|in:".implode(',', $types),
            'status' => "numeric|in:".implode(',', $status),
            'duration' => 'numeric',
            'is_milestone' => 'boolean',
            'is_sub_task' => 'boolean',
            'order' => 'numeric',
            'slug' => [
                'max:255',
                // Rule::unique('task_templates')->ignore($this->taskTemplate)
            ],
            'require_task_id' => 'numeric|exists:task_templates,id',
            'parent_task_id' => 'numeric|exists:task_templates,id',
            'sub_project_template_id' => 'required|exists:sub_project_templates,id'
        ];
    }
}
