<?php

namespace App\Http\Requests;

use App\Models\SubProjectPriority;
use App\Models\SubProjectStatus;
use App\Models\SubProjectType;

use Illuminate\Foundation\Http\FormRequest;

class SubProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $status = SubProjectStatus::listStatus();
        $types = SubProjectType::listTypes();
        $priorities = SubProjectPriority::listPriorities();

        return [
            'name' => 'required|max:255',
            'type' => "numeric|in:".implode(',', $types),
            'status' => "numeric|in:".implode(',', $status),
            'priority' => "numeric|in:".implode(',', $priorities),
            'duration' => 'numeric',
            'slug' => 'max:255',
            'duration' => 'numeric',
            'remaining_duration' => 'numeric',
            'total_tasks' => 'numeric',
            'remaining_tasks' => 'numeric',
            'sup_project_template_id'=> 'numeric',
            'client_id'=> 'numeric',
            'project_id'=> 'numeric',
            'assigned_to'=> 'numeric',
            'due_date' => 'date',
            'start_date'=> 'date',
            'started_at'=> 'date',
            'finished_at'=> 'date'
        ];
    }
}
