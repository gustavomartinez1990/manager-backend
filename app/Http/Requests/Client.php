<?php

namespace App\Http\Requests;

use App\Models\ClientStatus;
use App\Models\ClientType;
use Illuminate\Foundation\Http\FormRequest;

class Client extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $status = ClientStatus::listStatus();
        $types = ClientType::listTypes();

        return [
            'fiscal_name' => 'required|max:255',
            'commercial_name' => 'max:255',
            'nif' => 'max:100',
            'phone_number' => 'max:30',
            'email' => 'email:rfc',
            'logo_url' => 'max:255',
            'country'=> 'max:255',
            'city' => 'max:255',
            'street_address' => 'max:255',
            'type' => "numeric|in:".implode(',', $types),
            'status' => "numeric|in:".implode(',', $status),
            'referral_id' => "exists:clients,id",
            // 'referral_id' => "exists:App\Models\Client,id",
        ];
    }
}
