<?php

namespace App\Http\Requests;

use App\Models\SubProjectStatus;
use App\Models\SubProjectType;

use Illuminate\Foundation\Http\FormRequest;

class SubProjectTemplate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $status = SubProjectStatus::listStatus();
        $types = SubProjectType::listTypes();

        return [
            'name' => 'required|max:255',
            'description' => 'max:255',
            'type' => "numeric|in:".implode(',', $types),
            'status' => "numeric|in:".implode(',', $status),
            'duration' => 'numeric',
            'slug' => [
                'max:255',
                // Rule::unique('sub_project_templates')->ignore($this->subProjectTemplate)
            ],
            'total_tasks' => 'numeric',
        ];
    }
}
