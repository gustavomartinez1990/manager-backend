<?php

namespace App\Http\Requests;

use App\Models\TaskPriority;
use App\Models\TaskStatus;
use App\Models\TaskType;

use Illuminate\Foundation\Http\FormRequest;

class Task extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $priorities = TaskPriority::listPriorities();
        $status = TaskStatus::listStatus();
        $types = TaskType::listTypes();

        return [
            'name' => 'required|max:255',
            'description' => 'max:10000',
            'priority' => "required|numeric|in:".implode(',', $priorities),
            'type' => "required|numeric|in:".implode(',', $types),
            'status' => "required|numeric|in:".implode(',', $status),
            'duration' => 'numeric',
            'is_milestone' => 'boolean',
            'is_sub_task' => 'boolean',
            'order' => 'numeric',
            'due_date' => 'date',
            'start_date'=> 'date',
            'started_at'=> 'date',
            'finished_at'=> 'date',
            'require_task_id' => 'numeric|exists:tasks,id',
            'parent_task_id' => 'numeric|exists:tasks,id',
            'sub_project_id' => 'required|exists:sub_projects,id',
            'assigned_to' => 'required|exists:users,id',
        ];
    }
}
