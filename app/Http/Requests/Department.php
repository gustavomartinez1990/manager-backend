<?php

namespace App\Http\Requests;

use App\Models\DepartmentStatus;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Department extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $status = DepartmentStatus::listStatus();
        
        return [
            'name' => [
                'required',
                Rule::unique('departments')->ignore($this->department),
                'max:255'
            ],
            'description' => 'max:400',
            'status' => "numeric|in:".implode(',', $status),
        ];
    }
}
