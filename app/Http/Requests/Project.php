<?php

namespace App\Http\Requests;

use App\Models\ProjectPriority;
use App\Models\ProjectStatus;
use App\Models\ProjectType;

use Illuminate\Foundation\Http\FormRequest;

class Project extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $priorities = ProjectPriority::listPriorities();
        $status = ProjectStatus::listStatus();
        $types = ProjectType::listTypes();
        
        return [
            'name' => 'required|max:255',
            'description' => 'max:255',
            'documentation_url' => 'max:500',
            'type' => "numeric|in:".implode(',', $types),
            'status' => "numeric|in:".implode(',', $status),
            'priority' => "numeric|in:".implode(',', $priorities),
            'client_id' => 'required|numeric',
            'assigned_to' => 'numeric',
        ];
    }
}
