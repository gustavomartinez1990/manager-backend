<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\ClientController;
use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\Api\SubProjectTemplateController;
use App\Http\Controllers\Api\TaskTemplateController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\SubProjectController;
use App\Http\Controllers\Api\TaskController;
use App\Http\Controllers\Api\DashboardController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('auth')->group(function () {
    Route::middleware(['api'])->group(function () {
        Route::post('login', [AuthController::class, 'login']);
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('refresh', [AuthController::class, 'refresh']);
        Route::get('me', [AuthController::class, 'me']);
    });
});

Route::middleware(['auth:api'])->group(function () {
    
    // Users module routes.
    //Route::apiResource('users', UserController::class);
    Route::middleware(['permission:list_users'])->get('/users', [UserController::class, 'index']);
    Route::middleware(['permission:create_users'])->post('/users', [UserController::class, 'store']);
    Route::middleware(['permission:view_users'])->get('/users/{user}', [UserController::class, 'show']);
    Route::middleware(['permission:edit_users'])->put('/users/{user}', [UserController::class, 'update']);
    Route::middleware(['permission:delete_users'])->delete('/users/{user}', [UserController::class, 'destroy']);
    Route::middleware(['permission:edit_users'])->put('/users/change-password/{user}', [UserController::class, 'changePassword']);

    // Department module routes.
    Route::middleware(['permission:list_departments'])->get('/departments', [DepartmentController::class, 'index']);
    Route::middleware(['permission:create_departments'])->post('/departments', [DepartmentController::class, 'store']);
    Route::middleware(['permission:view_departments'])->get('/departments/{department}', [DepartmentController::class, 'show']);
    Route::middleware(['permission:edit_departments'])->put('/departments/{department}', [DepartmentController::class, 'update']);
    Route::middleware(['permission:delete_departments'])->delete('/departments/{department}', [DepartmentController::class, 'destroy']);
    
    // Client module routes.
    Route::middleware(['permission:list_clients'])->get('/clients', [ClientController::class, 'index']);
    Route::middleware(['permission:create_clients'])->post('/clients', [ClientController::class, 'store']);
    Route::middleware(['permission:view_clients'])->get('/clients/{client}', [ClientController::class, 'show']);
    Route::middleware(['permission:edit_clients'])->put('/clients/{client}', [ClientController::class, 'update']);
    Route::middleware(['permission:delete_clients'])->delete('/clients/{client}', [ClientController::class, 'destroy']);
    
    // SubProjectTemplate module routes.
    Route::middleware(['permission:list_template_tasks'])->get('/sub-project-template', [SubProjectTemplateController::class, 'index']);
    Route::middleware(['permission:create_template_tasks'])->post('/sub-project-template', [SubProjectTemplateController::class, 'store']);
    Route::middleware(['permission:view_template_tasks'])->get('/sub-project-template/{subProjectTemplate}', [SubProjectTemplateController::class, 'show']);
    Route::middleware(['permission:edit_template_tasks'])->put('/sub-project-template/{subProjectTemplate}', [SubProjectTemplateController::class, 'update']);
    Route::middleware(['permission:delete_template_tasks'])->delete('/sub-project-template/{subProjectTemplate}', [SubProjectTemplateController::class, 'destroy']);

    // TaskTemplate module routes.
    Route::middleware(['permission:list_template_tasks'])->get('/task-template', [TaskTemplateController::class, 'index']);
    Route::middleware(['permission:create_template_tasks'])->post('/task-template', [TaskTemplateController::class, 'store']);
    Route::middleware(['permission:view_template_tasks'])->get('/task-template/{taskTemplate}', [TaskTemplateController::class, 'show']);
    Route::middleware(['permission:edit_template_tasks'])->put('/task-template/{taskTemplate}', [TaskTemplateController::class, 'update']);
    Route::middleware(['permission:delete_template_tasks'])->delete('/task-template/{taskTemplate}', [TaskTemplateController::class, 'destroy']);
    Route::middleware(['permission:list_template_tasks'])->get('/sub-project-task-template/{subProjectTemplate}', [TaskTemplateController::class, 'subProjectTemplateTasks']);

    // Project module routes.
    Route::middleware(['permission:list_projects'])->get('/projects', [ProjectController::class, 'index']);
    Route::middleware(['permission:create_projects'])->post('/projects', [ProjectController::class, 'store']);
    Route::middleware(['permission:view_projects'])->get('/projects/{project}', [ProjectController::class, 'show']);
    Route::middleware(['permission:edit_projects'])->put('/projects/{project}', [ProjectController::class, 'update']);
    Route::middleware(['permission:delete_projects'])->delete('/projects/{project}', [ProjectController::class, 'destroy']);
    
    // Sub Project module routes.
    Route::middleware(['permission:list_subprojects'])->get('/sub-projects', [SubProjectController::class, 'index']);
    Route::middleware(['permission:create_subprojects'])->post('/sub-projects', [SubProjectController::class, 'store']);
    Route::middleware(['permission:view_subprojects'])->get('/sub-projects/{subProject}', [SubProjectController::class, 'show']);
    Route::middleware(['permission:edit_subprojects'])->put('/sub-projects/{subProject}', [SubProjectController::class, 'update']);
    Route::middleware(['permission:delete_subprojects'])->delete('/sub-projects/{subProject}', [SubProjectController::class, 'destroy']);
    Route::middleware(['permission:list_subprojects'])->get('/project-sub-projects/{project}', [SubProjectController::class, 'projectSubProjects']);

    // Task module routes.
    Route::middleware(['permission:list_tasks'])->get('/tasks/list/{subProject}', [TaskController::class, 'index']);
    Route::middleware(['permission:create_tasks'])->post('/tasks', [TaskController::class, 'store']);
    Route::middleware(['permission:view_tasks'])->get('/tasks/{task}', [TaskController::class, 'show']);
    Route::middleware(['permission:edit_tasks'])->put('/tasks/{task}', [TaskController::class, 'update']);
    Route::middleware(['permission:delete_tasks'])->delete('/tasks/{task}', [TaskController::class, 'destroy']);
    Route::middleware(['permission:edit_tasks'])->put('/task-change-status/{task}', [TaskController::class, 'changeStatus']);

    // Task module routes.
    Route::middleware(['permission:list_tasks'])->get('/dashboard', [DashboardController::class, 'stats']);
});

// handling all the routes that werent matched.
Route::fallback(function(){
    return response()->json([
        'message' => 'Page Not Found. If error persists, contact webdevelopergustavo@gmail.com'], 404);
});